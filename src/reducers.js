import {SET_EMAIL} from './actionType.js';

const email = (state = {}, action) => {
    switch(action.type) {
        case SET_EMAIL:
            return {
                email: action.email,
                valid: action.valid,
            };
        default: return state;
    }
};

export default email;