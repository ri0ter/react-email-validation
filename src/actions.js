import {SET_EMAIL} from './actionType';

const isValidEmail = (email) => {
    // NOTE: regex taken from default HTML5 email type check, works also with email type user@localhost
    return /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email);
};

export const setNewEmail = (email) => {
    return {
        type: SET_EMAIL,
        email: email,
        valid: isValidEmail(email),
    };
};