import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

import configureStore from './store.js'


const store = configureStore({
    email: '',
    valid: true
});

ReactDOM.render(
  <App store={store}/>,
  document.getElementById('root')
);
