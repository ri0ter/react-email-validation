import React, { Component } from 'react';
import { connect } from 'react-redux';
import TextField from './TextField'
import {setNewEmail} from './actions.js'
import './App.css';

class App extends Component {

  constructor(props) {
      super(props);
      this.onChange = this.onChange.bind(this);
  };

  onChange(email) {
      const store = this.props.store;
      store.dispatch(setNewEmail(email));
  };

  render() {

    const emailError = 'You must type in valid not empty email address';

    return (
      <div className="app">
          <form>
              <TextField
                  onChange={this.onChange}
                  isValid={this.props.valid}
                  value={this.props.email}
                  message={this.props.valid ? '' : emailError}
              />
              <button className="submit">Send</button>
          </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    const { email, valid } = state;
    return {
        email,
        valid,
    };
};

export default connect(mapStateToProps)(App);