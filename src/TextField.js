import React from 'react';
import { Component } from 'react';
import './TextField.css';

class TextField extends Component {

    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    };

    onChange = (event) => {
        this.props.onChange(event.target.value);
    };

    render() {

        const message= this.props.message;
        const isValid = this.props.isValid;

        return (
            <div className={'text-field' + (!isValid ? ' invalid' : '')}>
                <input className="input" type="text" placeholder="" onChange={this.onChange} />
                <span className="error">{message}</span>
            </div>
        );
    };
}

export default TextField;
